all: clean build

# 构建 web 端
build:
	mkdir -p public/ guix-vm/
	emacs --batch --no-init-file -l publish.el --eval "(org-publish-project \"os-note\")"
	emacs --batch --no-init-file -l publish.el config.org -f org-babel-tangle -f org-html-export-to-html

	tar -czvf install.tar.gz guix-vm
	mv install.tar.gz config.html public/
	cp website/* public/

clean:
	rm -rf public/
