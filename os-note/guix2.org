#+SETUPFILE: ./theme.org

* Guix 操作系统入门 (2)

* 手动全新安装 Guix 

之前我们尝试了直接使用图形界面 (TUI) 全新安装了 guix 系统。

下面我们尝试一下手动安装。

进入安装界面后, 再选择完地区和语言后, 这次不选择 TUI 形式安装, 而是选择下面的手动安装。

选择手动安装后, 会直接进入一个 shell。

按照提示, 可以尝试按下 alt+f2 查看文档 (和网页的内容一样的)

终端的文档界面是不是很熟悉? 试试按下 ~M-x~, 其实这个编辑器是 mg, 是 emacs 的前身

要操作时按下 ~alt+f3(~6)~ 就可以回到 shell

** 开启 SSHD 方便后续操作

先看看有没有 ip 地址 ~ifconfig~ (因为用的桥接模式, 所以应该是一个同网段的地址)

为了方便操作, 我们可以开启 sshd 这样就能用熟悉的设备登录进行操作了。

~herd status~ 看到 sshd stop 了, 先开启 ~herd start ssh-daemon~

还需要设置一下密码, 不然 ssh 无法登录 ~passwd root~

** 键盘和网络

键盘其实不用配置，绝大多数情况都是用的默认 layout。

网络由于使用的虚拟机 + 桥接模式， 也可以不配。

现阶段所有内容越简单越好, 先跑通了流程之后可以再优化。

** 分区配置

由于我们使用的是虚拟机, 就直接使用全部磁盘了。

列出可用磁盘 ~fdisk -l~, 我们只有一个磁盘, ~fdisk -l /dev/sda~ 看看具体信息

通过 ~fdisk /dev/sda~ 开始分区

|------+----------+-----------|
| sda1 | 1024M    | 启动分区  |
| sda2 | 2048M    | swap 分区 |
| sda3 | 剩余空间 | root 分区 |

创建文件系统 ~mkfs.fat -F32 /dev/sda1~ 

设置 swap 分区: ~mkswap /dev/sda2~, ~swapon /dev/sda2~

添加 label 方便后续操作 root 分区: ~mkfs.ext4 -L my-root /dev/sda3~

设置引导分区 ~parted /dev/sda set 1 esp on~

使用 ~lsblk -f~ 查看分区配置结果

最后挂载
#+BEGIN_SRC 
mount LABEL=my-root /mnt
mkdir -p /mnt/boot/efi
mkdir /mnt/etc
mount /dev/sda1 /mnt/boot/efi
#+END_SRC


* 准备配置文件

如果是用图形界面进行的配置, 最终它会安装用户的选择生成一个 ~config.scm~ 文件。

但我们要的是手动配置的话, 可以从 ~/etc/configuration/~ 目录中找到几个配置的模版。

这里我们就选用 desktop 这个配置作为初始内容, 开始进行配置。

* 配置镜像源

这次可以直接在安装以前配置好镜像源, 这样速度会快很多, 不用像之前图形界面安装时等那么久了。

** Channel

和之前的配置一样, 首先需要创建 ~/etc/channels.scm~ 配置文件

内容和之前一样:

#+begin_src lisp
  (list (channel
         (inherit (car %default-channels))
         (url "https://mirror.sjtu.edu.cn/git/guix.git")))
#+end_src

** Substitute

除了直接修改配置文件以外, 还是需要设置一下环境变量, 理由和之前一样

#+begin_src lisp
(services (modify-services %desktop-services
              (guix-service-type
               config => (guix-configuration
                          (inherit config)
                          (substitute-urls '("https://mirror.sjtu.edu.cn/guix/"
                                             "https://ci.guix.gnu.org"))))))
#+end_src

~export GUIX_BUILD_OPTIONS="--substitute-urls=https://mirror.sjtu.edu.cn/guix/"~

* 配置 bootloader

可以通过查看 ~lsblk -f~ 命令来获取引导分区的 id, 然后在 ~config.scm~ 中替换为自己的 id

#+begin_src lisp
(file-system
  (device (uuid "AA22-46FB" 'fat))
    (mount-point "/boot/efi")
    (type "vfat"))
#+end_src

* 安装与启动

至此, 我们已经有了配置好镜像的 ~channels.scm~ 和经过修改的 ~config.scm~, 下面就要开始安装了。

主要就是下面这几步:

+ herd start cow-store /mnt # 让下载的 store 内容也放到挂载的 root 分区中, 不然会磁盘空间不足
+ cp /etc/channels.scm /mnt/etc/  # 把我们安装镜像中配置的 channels 复制到挂载的文件系统中
+ cp /path/to/config.scm /mnt/etc/config.scm # 把配置好的 config 文件复制到挂载的文件系统中, 比如 /etc/configuration/desktop.scm
+ guix time-machine -C /mnt/etc/channels.scm -- system init /mnt/etc/config.scm /mnt # 安装 guix 操作系统
+ reboot # 重启
