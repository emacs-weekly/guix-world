(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

(package-initialize)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package org
  :ensure t)

(use-package htmlize
  :ensure t
  :config
  (setq org-html-htmlize-output-type 'css)
  (setq org-html-head-include-default-style nil))

(require 'org)
(require 'ox-publish)

(setq org-confirm-babel-evaluate nil)
(setq org-export-babel-evaluate t)

(setq org-publish-project-alist
      '(("os-note"
	 :base-directory "os-note"
	 :publishing-function org-html-publish-to-html
	 :publishing-directory "public/os-note"
	 :exclude "theme.org"
	 :section-numbers nil
	 :with-toc nil)))

(provide 'publish)

